using UnityEngine;

public class CarController : MonoBehaviour
{
    [Tooltip("(m/sec)")]
    [Range(0, 5)]
    [SerializeField] private float maxSpeed = 1f;

    [Tooltip("degree/sec")]
    [SerializeField] private float maxRotationSpeed = 200f;

    [Tooltip("(sec)")]
    [Range(0, 1)]
    [SerializeField] private float wheelTimeToMax = 0.1f;

    [Space]
    [SerializeField] private GameObject targetObj;

    [SerializeField] private GameObject wallPrefab;
    [SerializeField] private GameObject wallCornerPrefab;
    [SerializeField] private Material wallRegularMat;

    [HideInInspector] public bool shouldRecord = false;

    [Space]
    [SerializeField] private float speed = 0f;
    [SerializeField] private float rotationSpeed = 0f;

    private Vector3 direction;
    private Vector3 rotation;
    private bool wasRotating = true;

    private GameObject curWall;
    private GameObject curWallCorner;
    private Transform perimeter;
    private Transform wallStart;
    private Vector3 buildStartPos;
    private bool isBuildingClosingWall = false;

    private Vector3 targetPos;
    private Quaternion targetRot;
    private sbyte rotDirToTarget = 0;
    private bool moveToTarget = false;

    private void Awake()
    {
        perimeter = GameObject.FindWithTag("Perimeter").transform;
        wallStart = GameObject.FindWithTag("WallStart").transform;
        targetObj.SetActive(false);
    }

    private void Update()
    {
        direction = Vector3.zero;
        rotation = Vector3.zero;

        if (shouldRecord)
        {
            if (IsPressingForward()) direction += Vector3.forward;
            if (IsPressingBackward()) direction += Vector3.back;

            if (IsPressingLeft()) rotation.y -= 1;
            if (IsPressingRight()) rotation.y += 1;

            if (direction != Vector3.zero) rotation = Vector3.zero;

            RecordPerimeter();
        }
        else
        {
            if (moveToTarget)
            {
                direction = Vector3.forward;
            }
            else if (rotDirToTarget == 1 || rotDirToTarget == -1)
            {
                rotation.y = rotDirToTarget;
            }
            else if (!isBuildingClosingWall && !Utils.IsMouseOverUI() && Input.GetMouseButtonDown(0))
            {
                Vector3 mouseWorldPos = Utils.GetMousePosInWorld(transform.position.y);
                if (IsPointInsidePerimeter(mouseWorldPos, 12)) StartGoingToPoint(mouseWorldPos);
            }

            if (isBuildingClosingWall) RecordPerimeter();
        }

        Move();

        wasRotating = (rotation.y != 0);
    }

    private bool IsPressingForward() => (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow));

    private bool IsPressingBackward() => (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow));

    private bool IsPressingLeft() => (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow));

    private bool IsPressingRight() => (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow));

    private void RecordPerimeter()
    {
        if (rotation.y == 0 && wasRotating)
        {
            if (curWall != null)
            {
                if (curWall.transform.localScale.z == 0)
                {
                    Destroy(curWall);
                    if (curWallCorner != null) Destroy(curWallCorner);
                }
                else
                {
                    EndCurWallBuild();
                }
            }
            curWall = Instantiate(wallPrefab, wallStart.position, transform.rotation);
            curWallCorner = Instantiate(wallCornerPrefab, new Vector3(wallStart.position.x, curWall.transform.position.y, wallStart.position.z), Quaternion.identity);
        }
    }

    private void Move()
    {
        if (rotation == Vector3.zero)
        {
            ArduinoController.StopRotating();

            rotationSpeed = 0;
        }
        else
        {
            rotationSpeed = Mathf.Clamp(rotationSpeed + Time.deltaTime * maxRotationSpeed / wheelTimeToMax, 0f, maxRotationSpeed);
        }
        if (direction == Vector3.zero)
        {
            ArduinoController.StopMoving();

            speed = 0;
        }
        else
        {
            speed = Mathf.Clamp(speed + Time.deltaTime * maxSpeed / wheelTimeToMax, 0f, maxSpeed);
        }

        if (rotation.y == -1) ArduinoController.RotateLeft();
        else if (rotation.y == 1) ArduinoController.RotateRight();

        if (direction == Vector3.forward) ArduinoController.MoveForward();
        else if (direction == Vector3.back) ArduinoController.MoveBackward();


        transform.Translate(Time.deltaTime * speed * direction);
        transform.Rotate(Time.deltaTime * rotationSpeed * rotation);
    }

    private bool IsPointInsidePerimeter(Vector3 point, int rayCount = 10)
    {
        bool isInsidePerimeter = true;
        for (int i = 0; i < rayCount; ++i)
        {
            Vector3 dir = Utils.AngleToVector(i * 360f / rayCount);
            Debug.DrawRay(point, dir * 50, Color.green, 0.15f);
            if (!Physics.Raycast(point, dir, out _, Mathf.Infinity))
            {
                isInsidePerimeter = false;
                break;
            }
        }
        return isInsidePerimeter;
    }

    private void StartGoingToPoint(Vector3 point)
    {
        point.y = transform.position.y;
        if (transform.position == point)
        {
            moveToTarget = false;
            rotDirToTarget = 0;
            return;
        }

        targetObj.transform.position = point;
        targetObj.SetActive(true);

        targetPos = point;
        targetRot = Quaternion.LookRotation(point - transform.position);

        float d = (targetRot.eulerAngles.y - transform.rotation.eulerAngles.y);
        if (d < 0)
        {
            d = (360 - transform.rotation.eulerAngles.y) + targetRot.eulerAngles.y;
        }
        if (d > 180)
        {
            d = 360 - d;
            rotDirToTarget = -1;
        }
        else
        {
            rotDirToTarget = 1;
        }
        Invoke(nameof(StopRotatingToTarget), CalculateTimeToRot(d));

        float CalculateTimeToRot(float degree)
        {
            float maxRotSpeedAount = 0.5f * wheelTimeToMax * maxRotationSpeed;
            if (degree < maxRotSpeedAount)
            {
                return Mathf.Sqrt(2 * degree * wheelTimeToMax / maxRotationSpeed);
            }
            return ((degree - maxRotSpeedAount) / maxRotationSpeed) + wheelTimeToMax;
        }
    }

    private void StopRotatingToTarget()
    {
        rotDirToTarget = 0;
        transform.rotation = targetRot;

        moveToTarget = true;
        Invoke(nameof(StopMovingToTarget), CalculateTimeToCrossDist((targetPos - transform.position).magnitude));

        float CalculateTimeToCrossDist(float dist)
        {
            float maxSpeedDist = 0.5f * wheelTimeToMax * maxSpeed;
            if (dist < maxSpeedDist)
            {
                return Mathf.Sqrt(2 * dist * wheelTimeToMax / maxSpeed);
            }
            return ((dist - maxSpeedDist) / maxSpeed) + wheelTimeToMax;
        }
    }

    private void StopMovingToTarget()
    {
        moveToTarget = false;
        transform.position = targetPos;
        targetObj.SetActive(false);

        if (isBuildingClosingWall)
        {
            isBuildingClosingWall = false;
            GameObject.FindWithTag("RecordButton").GetComponent<UnityEngine.UI.Button>().interactable = true;
            GameObject.FindWithTag("AutoCloseToggle").GetComponent<UnityEngine.UI.Toggle>().interactable = true;
            StopRecording(true);
        }
    }

    private void EndCurWallBuild()
    {
        if (curWall != null)
        {
            curWall.GetComponent<MeshRenderer>().material = wallRegularMat;
            curWall.GetComponent<WallBuilder>().enabled = false;
            curWall.transform.parent = perimeter;
        }
        if (curWallCorner != null)
        {
            curWallCorner.GetComponent<MeshRenderer>().material = wallRegularMat;
            curWallCorner.transform.parent = perimeter;
        }
        curWall = null;
        curWallCorner = null;
    }

    public void StopRecording(bool skipDistCheck = false)
    {
        if (skipDistCheck)
        {
            EndCurWallBuild();
        }
        else
        {
            if ((wallStart.position - buildStartPos).sqrMagnitude > Utils.Squaref((wallCornerPrefab.transform.localScale.x / 2) - 0.01f))
            {
                if (wallStart.localPosition.z == 0)
                {
                    StartGoingToPoint(buildStartPos);
                }
                else
                {
                    StartGoingToPoint(buildStartPos + ((transform.position - buildStartPos).normalized * wallStart.localPosition.z));
                }
                isBuildingClosingWall = true;
                GameObject.FindWithTag("RecordButton").GetComponent<UnityEngine.UI.Button>().interactable = false;
                GameObject.FindWithTag("AutoCloseToggle").GetComponent<UnityEngine.UI.Toggle>().interactable = false;
            }
            else
            {
                EndCurWallBuild();
            }
        }
        shouldRecord = false;
    }

    public void StartRecording()
    {
        buildStartPos = wallStart.position;
        wasRotating = true;
        shouldRecord = true;
    }
}
