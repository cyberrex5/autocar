﻿using UnityEngine;

public static class Utils
{
    private static Camera cam;

    [RuntimeInitializeOnLoadMethod]
    private static void Start()
    {
        cam = Camera.main;
    }

    public static Vector3 GetMousePosInWorld(float offset = 0.0f)
    {
        Vector3 mousePos = Input.mousePosition;
        return cam.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, cam.gameObject.transform.position.y - offset));
    }

    public static bool IsMouseOverUI() => UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject();

    public static Vector3 AngleToVector(float angle)
    {
        float rad = angle * Mathf.PI / 180;
        return new Vector3(Mathf.Cos(rad), 0.0f, Mathf.Sin(rad));
    }

    public static float Squaref(float f) => f * f;
}
