using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private float zoomSpeed = 1f;

    private Vector3 initialPos;

    private void Update()
    {
        Vector3 newPos = transform.position;
        float newCamHeight = Mathf.Clamp(transform.position.y - (Input.mouseScrollDelta.y * zoomSpeed), 1f, 50f);
        if (Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2))
        {
            initialPos = Utils.GetMousePosInWorld();
        }
        if (Input.GetMouseButton(1) || Input.GetMouseButton(2))
        {
            newPos -= Utils.GetMousePosInWorld() - initialPos;
        }
        newPos.y = newCamHeight;
        transform.position = newPos;
    }
}
