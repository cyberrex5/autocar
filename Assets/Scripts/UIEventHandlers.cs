using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class UIEventHandlers : MonoBehaviour
{
    public string RecordButtonStartTxt = "Record";
    public string RecordButtonStopTxt = "Stop Recording";

    private CarController carScript;

    private void Awake()
    {
        carScript = GameObject.FindWithTag("Car").GetComponentInParent<CarController>();
    }

    public void OnRecordClick(TextMeshProUGUI tmp)
    {
        if (carScript.shouldRecord)
        {
            carScript.StopRecording(!GameObject.FindWithTag("AutoCloseToggle").GetComponent<Toggle>().isOn);
            tmp.text = RecordButtonStartTxt;
        }
        else
        {
            carScript.StartRecording();
            tmp.text = RecordButtonStopTxt;
        }
    }

    public void OnResetClick()
    {
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
    }
}
